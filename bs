#!/usr/bin/python3
# -*- python -*- -*- coding: utf-8 -*-
#
# Boot shell
#
# GPLv3

import proxy
import sys
import getopt
import projects

class Interceptor(proxy.Interceptor):
    def __init__(m):
        m.forget()
        proxy.Interceptor.__init__(m)

    def forget(m):
        m.inflight = ""

    def have(m, l):
        return proxy.findlast(m.inflight, l)

    def master_read(m, data):
        '''
        Called when there is data to be sent from the child process back to the user.
        '''
        m.inflight += data
        m.state_machine.__next__()

        m.write_stdout(data)

def help():
    print('atest.py [...]')
    sys.exit(2)

def main(argv):
    try:
        opts, args = getopt.gnu_getopt(argv, "hm:", [ "machine=" ])
    except getopt.GetoptError:
        help()

    command = ""
    machine = ""
    
    if args != []:
        command = args[0]

    for opt, arg in opts:
        if opt == '-h':
            help()
        elif opt in ("-m", "--machine"):
            machine = arg

    iteration = projects.MyIteration(machine)
    iteration.machine.reboot()

    print("Command is ", command, "\n")

    if command == "":
        command = "linux"

    i = Interceptor()
    i.state_machine = iteration.machine.run(i)
    i.write_stdout('\nThe dream has begun.\n')

    if command in [ "u-boot", "u-boot2", "u-boot3", "linux" ]:
        iteration.machine.stop_at = command
        i.spawn(iteration.machine.connect())

    i.write_stdout('\nThe dream is (probably) over.\n')

if __name__ == "__main__":
   main(sys.argv[1:])

"""
   serial settings:

speed 115200 baud; rows 50; columns 88; line = 0;
intr = ^C; quit = ^\; erase = ^?; kill = ^U; eof = ^D; eol = <undef>; eol2 = <undef>;
swtch = <undef>; start = ^Q; stop = ^S; susp = ^Z; rprnt = ^R; werase = ^W; lnext = ^V;
flush = ^O; min = 1; time = 5;
-parenb -parodd cs8 -hupcl -cstopb cread clocal -crtscts
ignbrk -brkint -ignpar -parmrk -inpck -istrip -inlcr -igncr -icrnl -ixon -ixoff -iuclc
-ixany -imaxbel -iutf8
-opost -olcuc -ocrnl -onlcr -onocr -onlret -ofill -ofdel nl0 cr0 tab0 bs0 vt0 ff0
-isig -icanon -iexten -echo -echoe -echok -echonl -noflsh -xcase -tostop -echoprt
-echoctl -echoke

"""
