#!/usr/bin/python3
# This file will contain stuff specific for your projects.

import base
import os
import time
import sys

class Environment:
    def power(m, machine, on):
        print("Please power ", machine.target, on)

    def connect(m, machine):
        print("Please configure how to connect to target.")
        raise "This is mandatory."

class Denx(Environment):
    def power(m, machine, on):
        base.cmd("remote_power "+machine.target+" "+on)
        time.sleep(1)

    def connect(m, machine):
        return ["connect", machine.target]

class Ricany(Environment):
    def __init__(m):
        m.target_ip = "10.0.0.99"
        m.tftp_server = "10.0.0.6"

    def power(m, machine, on):
        print("Please power machine "+machine.target+" "+on)
        if on == "on":
            #print("Press enter when ready")
            #sys.stdin.readline()
            #print("okay...")
            pass

    def connect(m, machine):
        return "/dev/ttyUSB0"

class Wagabuibui_simple(base.Tftpboot2):
    def __init__(self):
        self.target = "wagabuibui"
        base.Tftpboot2.__init__(self)
        self.name = self.target
        self.srcdir = "/data/denx/wagabuibui/linux"
        self.machine = "sockit"
#        self.machine = "wagabuibui"
        self.env = Ricany()

    def start_kernel(m):
        yield from m.cmd_uboot("setenv bootargs console=ttyS0,115200 mem=128M@1M rootdelay=1 root=/dev/mmcblk0p2 earlyprintk")
        yield from m.cmd_uboot("tftp 3000000 "+m.target+"/uImage.bb")
        yield from m.cmd_uboot("tftp 2000000 "+m.target+"/dtb.bb")
        yield from m.cmd_uboot("mmc part")
        m.write_target("bootm 3000000 - 2000000\n")

    def setup_uboot_v4l(m):
        yield from m.cmd_uboot("setenv fpgadata_addr 0x2000000")
        yield from m.cmd_uboot("setenv prepare_fpga 'fatload mmc 0:1 $fpgadata_addr current_mvs_hardware.rbf; fpga load 0 $fpgadata_addr $filesize'")
        yield from m.cmd_uboot("fatload mmc 0:1 0x6000000 test_init_script.ub; source 0x6000000")

    def setup_uboot(m):
        # JPEG grabber setup
        yield from m.cmd_uboot("setenv fpgadata_addr 0x2000000")
        yield from m.cmd_uboot("setenv prepare_fpga 'fatload mmc 0:1 $fpgadata_addr current_streaming_hardware.rbf; fpga load 0 $fpgadata_addr $filesize'")
        yield from m.cmd_uboot("run prepare_fpga")
        yield from m.cmd_uboot("fatload mmc 0:1 0x14c00000 current_streaming_software.bin; run prepare_fpga")
        yield from m.cmd_uboot("bridge isw 3 0x377; bridge e")
        #yield from m.cmd_uboot("setenv fpga2sdram_handoff 377; run bridge_enable_handoff")
        # "bridge e" in new u-boot.
        # run bridge_enable_handoff

class Stahl_simple(base.Tftpboot2):
    def __init__(self):
        self.target = "stahl"
        base.Tftpboot2.__init__(self)
        self.name = self.target
        self.srcdir = "/data/denx/stahl/linux"
        self.machine = "sockit"
        self.env = Ricany()

    def setup_uboot(m):
        # JPEG grabber setup
        yield from m.cmd_uboot("echo ok")

# MACHINE=qemuarm bitbake wagabuibui \
# rm -r root root.pseudo_state/  && \
# runqemu-extract-sdk tmp/deploy/images/qemuarm/wagabuibui-qemuarm.tar.gz root/
# MACHINE=qemuarm runqemu root/ nographic
# MACHINE=qemuarm bitbake meta-toolchain-sdk
#  meta-ide-support

class MyIteration(base.Iteration):
    machines = [ Wagabuibui_simple(), Stahl_simple() ]

    


