
    def progress_linux_to_userland(self):
        yield from self.wait_msg("image load", "Booting kernel from Legacy Image", 30)
        print("### waiting for linux")
        yield from self.wait_msg("Linux starts", "Kernel command line:", 60)
        print("### waiting for root")
        yield from self.wait_msg("Mounts root", "VFS: Mounted root", 60)
        yield from self.wait_msg("Userland boots", "Stopping Bootlog daemon:", 60)

    def nfsroot_cmdline(self):
        return "rw nfsroot="+self.tftp_server+":"+self.nfsroot_dir+",nfsvers=3 ip="+self.target_ip+":"+self.tftp_server+":"+self.tftp_server+":"+self.target_netmask+":dummy:eth0:off"

    def setup_server(self):
        yield from self.cmd_uboot("setenv serverip "+self.tftp_server)
        yield from self.cmd_uboot("setenv ipaddr "+self.target_ip)

    def start_kernel_from_tftp(self, cmdline_root):
        yield from self.setup_server()
        yield from self.cmd_uboot("tftp %s %s/dtb.bb" % (self.dtb_load_addr, self.target), 30)
        yield from self.cmd_uboot("tftp %s %s/uImage.bb" % (self.kernel_load_addr, self.target), 30)
        yield from self.start_kernel_common(cmdline_root)
        time.sleep(.1)
        self.write_target("bootm %s - %s\n" % (self.kernel_load_addr, self.dtb_load_addr))

class VlabBitbake(Tftpboot):
    def __init__(self):
        Tftpboot.__init__(self)
        self.target_netmask = "255.255.0.0"
        self.tftp_server = "192.168.1.1"
        self.tftp_dir = "/tftpboot/"+self.target

class MyBitbake(VlabBitbake):
    def __init__(self):
        self.srcdir = "/home/pavel/"+self.target+"/eldk"
        self.mydir = "/home/pavel/auto"
        VlabBitbake.__init__(self)
        self.name = self.target
        self.worker_host = "pavel@sisyphus"
        self.worker_dir = "/work/pavel/build-"+self.target
        self.nfsroot_dir = "/work/pavel/root-"+self.target

class Stahl(MyBitbake):
    # MACHINE=generic-armv7a bitbake core-image-minimal
    def __init__(self):
        #self.target = "wagabuibui"
        MyBitbake.__init__(self)
        self.srcdir = "/home/pavel/stahl/eldk"
        self.worker_host = "pavel@sisyphus"
        #self.deploy_images = self.worker_dir + "/tmp/deploy/images/sockit" 
        #self.deploy_kernel = self.deploy_images + "/uImage-sockit.bin"
        self.deploy_uboot = None # self.deploy_images + "/u-boot.bin"
        self.deploy_dtb = None # self.worker_dir + "/tmp/work/generic_powerpc-linux/linux-ib8315-3.8.2-ib-r1/git/arch/powerpc/boot/tqm8315.dtb"
        #self.target_ip = "192.168.20.77"
        #self.machine = "sockit"
        #self.bitbake_target = "wagabuibui-image-devel"
        #self.worker_image = self.deploy_images + "/wagabuibui-image-devel-sockit"

        kernel_dir = "/home/pavel/wagabuibui/linux/"
        self.deploy_kernel = kernel_dir + "arch/arm/boot/uImage"
        self.deploy_dtb = kernel_dir + "arch/arm/boot/dts/socfpga_cyclone5_sockit.dtb"

        # 32M : 0x2000000
        self.kernel_load_addr = "0x3000000"
        self.dtb_load_addr = "0x2000000"
        self.env = Denx()


class Wagabuibui(MyBitbake):
    # MACHINE=generic-armv7a bitbake core-image-minimal
    def __init__(self):
        self.target = "wagabuibui"
        MyBitbake.__init__(self)
        self.srcdir = "/home/pavel/wagabuibui/eldk"
        self.worker_host = "pavel@sisyphus"
        self.deploy_images = self.worker_dir + "/tmp/deploy/images/sockit" 
        self.deploy_kernel = self.deploy_images + "/uImage-sockit.bin"
        self.deploy_uboot = None # self.deploy_images + "/u-boot.bin"
        self.deploy_dtb = None # self.worker_dir + "/tmp/work/generic_powerpc-linux/linux-ib8315-3.8.2-ib-r1/git/arch/powerpc/boot/tqm8315.dtb"
        self.target_ip = "192.168.20.77"
        self.machine = "sockit"
#        self.machine = "wagabuibui"
        self.bitbake_target = "wagabuibui-image-devel"
        self.worker_image = self.deploy_images + "/wagabuibui-image-devel-sockit"

        kernel_dir = "/home/pavel/wagabuibui/linux/"
        self.deploy_kernel = kernel_dir + "arch/arm/boot/uImage"
        self.deploy_dtb = kernel_dir + "arch/arm/boot/dts/socfpga_cyclone5_sockit.dtb"

        # 32M : 0x2000000
        self.kernel_load_addr = "0x3000000"
        self.dtb_load_addr = "0x2000000"
        self.env = Denx()

    def start_kernel_common(self, cmdline_root):
        print("setting up command line")
        yield from self.cmd_uboot("setenv bootargs "+cmdline_root+" "+self.nfsroot_cmdline()+" console=ttyS0,57600 mem=256M")
        print("done setting up command line")

    def start_kernel_from_flash(self, cmdline_root):
        self.start_kernel_common(cmdline_root)
        yield from self.cmd_uboot("mmc rescan;${mmcloadcmd} mmc 0:${mmcloadpart} ${loadaddr} ${bootimage};${mmcloadcmd} mmc 0:${mmcloadpart} ${fdtaddr} ${fdtimage}")
        time.sleep(.1)
        self.write_target("bootm ${loadaddr} - ${fdtaddr}\n")

    def wait_for_uboot(self):
        yield from self.wait_msg("u-boot12", "Net:   mii0")
        time.sleep(1)
        self.write_target("stop\n")
        time.sleep(1)

    def custom_uboot(self):
        yield from self.cmd_uboot("fatload mmc 0:1 $fpgadata cur/final_arrow.rbf; fpga load 0 $fpgadata $filesize")
        self.write_target("reset\n")
        yield from self.wait_for_uboot()
        yield from self.cmd_uboot("fatload mmc 0:1 0x01000040 my/u-boot.bin")
        self.write_target("go 0x01000040\n")
        yield from self.wait_for_uboot()
        if self.stop_at == "u-boot2":
            print("Giving you controls\n")
            yield from m.your_controls()
        yield from self.cmd_uboot("fatload mmc 0:1 0x14000000 cur/final_arrow.bin; setenv fpga2sdram_handoff 377; run bridge_enable_handoff")
        yield from self.cmd_uboot("md.l 0x10000000 24")

    def start_kernel(self):
        yield from self.custom_uboot()
        yield from self.tertiary_uboot()
        yield from self.start_kernel_from_tftp("root=/dev/nfs rootdelay=5 irqpoll")

#        self.start_kernel_from_flash("rootdelay=3 root=/dev/mmcblk0p2")

