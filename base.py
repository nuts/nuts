#!/usr/bin/python
# This should only contain stuff generic / important for everyone.

import os
import time

def pcmd(c):
    return os.popen(c).readline()[:-1]

def cmd(c):
    ret = os.system(c)
    if ret != 0:
        raise Exception("Subcommand failed")

class Iteration:
    def get_machine(self, name):
        for m in self.machines:
            if name == m.name:
                os.chdir(m.srcdir)
                return m
        if name == "":
            d = os.getcwd()
            for m in self.machines:
                print(m.name, d, m.srcdir)
                if d == m.srcdir:
                    return m
        raise Exception("Machine not specifed and not in suitable srcdir")
        
    def __init__(self, name):
        print("User asks for ", name)
        self.machine = self.get_machine(name)
        self.dir = self.machine.srcdir + "/cur_test"
    
class Expect:
    def __init__(m):
        m.uboot_clock = 0
        m.stop_at = None

    def your_controls(m):
        print("### Your controls\n")
        while True:
            yield

    def wait_msg(m, phase, regexp, timeout = 10):
        i = m.interceptor
        while True:
            if i.have( [ regexp ] ):
                print("\n### Milestone reached:", phase)
                if phase == m.stop_at:
                    yield from m.your_controls()
                break
            yield

    def write_target(m, s):
        m.interceptor.write_master(s)

    def cmd_any(self, s, msg, timeout = 10, wait_key = ""):
        self.last_output = []
        key_w = "Proc''eed%03d" % self.uboot_clock
        key = "Proceed%03d" % self.uboot_clock
        self.uboot_clock += 1
        time.sleep(.1)
        self.write_target(s % key_w)
        yield from self.wait_msg(msg, wait_key+key, timeout)

    # u-boot does not implement reasonable flow control; try talking slowly
    def cmd_uboot(self, s, timeout = 10):
        time.sleep(.1)
        yield from self.cmd_any(s + " && echo %s\n", "u-boot talk", timeout)

    def cmd_shell(self, s, timeout = 10):
        yield from self.cmd_any(s + " && echo %s\n", "shell talk", timeout)

class Tftpboot(Expect):
    def power_cycle(m):
        m.env.power(m, "off")
        m.env.power(m, "on")

    def reboot(self):
        self.power_cycle()

    def connect(m):
        return m.env.connect(m)

    def stop_uboot(m):
        i = m.interceptor
        while True:
            if i.have( [ 'Hit any key to stop autoboot' ] ):
                i.forget()
                i.write_master('x')
                break
            yield

    def setup_uboot(m):
        pass

    def start_kernel(m):
        yield from m.cmd_uboot("setenv bootargs console=ttyS0,115200 mem=240M rootdelay=5 root=/dev/sda1 earlyprintk init=/bin/bash")
        yield from m.cmd_uboot("tftp 3000000 "+m.target+"/uImage.bb")
        yield from m.cmd_uboot("tftp 2000000 "+m.target+"/dtb.bb")
        m.write_target("bootm 3000000 - 2000000\n")

    def setup_server(m):
        yield from m.cmd_uboot("setenv ethaddr 12:34:56:78:90:ab; true")
        yield from m.cmd_uboot("setenv serverip "+m.env.tftp_server)
        yield from m.cmd_uboot("setenv ipaddr "+m.env.target_ip)

# tftp 0x01000040 u-boot.bin
# setenv serverip 10.0.0.6
# setenv ipaddr 10.0.0.99

# Boot using secondary u-boot.
class Tftpboot2(Tftpboot):
    def secondary_uboot(m):
        yield from m.cmd_uboot("tftp 0x01000040 "+m.target+"/u-boot.bin")
        m.write_target("go 0x01000040\n")
        yield from m.stop_uboot()
        yield from m.setup_server()

    def run(m, i):
        m.interceptor = i
        yield from m.stop_uboot()
        print("have u-boot, should stop at '%s'\n" % m.stop_at)
        yield from m.setup_server()
        if m.stop_at == "u-boot":
            print("Giving you controls\n")
            yield from m.your_controls()

        yield from m.secondary_uboot()
        if m.stop_at == "u-boot2":
            print("Giving you controls\n")
            yield from m.your_controls()

        yield from m.setup_uboot()
        yield from m.start_kernel()
        #yield from m.progress_linux_to_userland()

        while True:
            if i.have( [ 'login:' ] ):
                i.forget()
                i.write_master('root\n')
                if m.stop_at == "linux":
                    yield from m.your_controls()
            yield




