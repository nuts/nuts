#!/usr/bin/python
#
# Scripts for automatic kernel testing
# 
# Copyright 2013 Pavel Machek <pavel@ucw.cz>
# Distribute under GPLv2+
#
import os
import re
import sys
import time
import sys, getopt
import subprocess
import signal

def pcmd(c):
    return os.popen(c).readline()[:-1]

def cmd(c):
    ret = os.system(c)
    if ret != 0:
        raise Exception("Subcommand failed")

class Test:
    pass

#
# Nokia N900
#

class Search_text(Test):
    def search(self, filename, regexps):
        data = open(filename).readlines()
        for line in data:
            for reg in regexps:
                if re.match(reg, line): return 1
        return 0

class Boots_emu(Search_text):
    name = "Waits for root device on emulator"

    def run(self, iteration):
        res = self.search("dmesg.qemu",
              [ "Waiting for root device.", 
                "Unable to mount root fs." ])
        if res: return "pass"
        return "FAIL"

class I2C_irq_ok(Boots_emu):
    name = "I2C does not report problems with too much irq work"

    def run(self, iteration):
        data = open("dmesg.qemu").readlines()
        for line in data:
            if re.match("^.*Too much work in one IRQ*$", line): return "FAIL"
        res = Boots_emu.run(self, iteration)
        if res == "FAIL":
            return "UNKNOWN"
        return "pass"

class N900:
    # eldk can't compile 2.6.28; just use make, it is set up in makefile
#  /data/l/maemo/kernel-power/kernel-power-2.6.28
#    srcdir = "/data/l/maemo/kernel-power/kernel-power-2.6.28"
    srcdir = "/data/l/linux-n900"
    mydir = "/data/l/auto"
    qemudir = "/data/l/maemo/qemu"
    name = "n900"

    def add_dtb(self):
        os.system("cat arch/arm/boot/zImage arch/arm/boot/dts/omap3-n900.dtb > zImage_dtb")

    def build(self, iteration):
        os.chdir(self.srcdir)
        
        cmd("""bash -c '. eldk-switch.sh -r 5.4 armv7a; ARCH=arm nice time make -j 3 zImage modules'""")
        self.add_dtb()

    def build_dist(self, iteration):
        os.chdir(self.srcdir)
        os.system('ARCH=arm DISTCC_HOSTS="duo localhost" nice time make -j 8 CC="distcc arm-linux-gnueabi-gcc"')
        self.add_dtb()

    def run_qemu(self):
        cmd("%s/qflasher -m rx51 -x %s/xloader-qemu.bin -s %s/secondary-qemu.bin -k zImage_dtb -o n900.img -p k=16072" % 
            (self.qemudir, self.qemudir, self.qemudir))
        os.system("%s/qemu-system-arm -s -M n900 -mtdblock n900.img -sd /data/l/maemo/mmcblk1.img -serial stdio -clock unix -redir tcp:5555:10.0.2.15:22 | tee dmesg.qemu.dirty &" % self.qemudir)

    def run_emu(self, iteration):
        os.chdir(self.srcdir)
        os.system("rm dmesg.qemu.dirty")
        self.run_qemu()
        # os.system("(( cd ../maemo/qemu; ./run ) 2>&1 | tee dmesg.qemu.dirty ) &")
        time.sleep(40)
        print "Killing emulator"
        os.system("killall qemu-system-arm")
        time.sleep(1)
        os.system("cat dmesg.qemu.dirty | sed 's/.*\]//' | grep -v '\(OneNAND blocks unlocked\|Logo drawn in\|ber-cool backlight fade-in\|Loading kernel\|Total bootup time\|Calibrating delay loop\|Linux version 3\|Memory:\|text :\|init :\|data :\|bss :\|free_area_init_node:\|Setting up static identity map\)' > dmesg.qemu")

    def run_hw(self, iteration):
        os.chdir(self.srcdir)
#        cmd("sudo ../maemo/0xffff/src/0xFFFF -m zImage_dtb -l -b 'root=/dev/mmcblk0p6 rootwait'")
        version = pcmd("sed -n 'N;s/^VERSION = //;s/\\nPATCHLEVEL = /./p' Makefile")
        print version
        cmd("rm -rf /tmp/kernel-modules")
        cmd("mkdir /tmp/kernel-modules")
#  INSTALL_MOD_STRIP=1
        print "Making modules"
        cmd("ARCH=arm make modules_install INSTALL_MOD_PATH=/tmp/kernel-modules/")
        str=pcmd("ls /tmp/kernel-modules/lib/modules/")
        print str
        os.chdir("/tmp/kernel-modules/lib/modules/%s" % str)
        print "removing"
        cmd("bash -c 'rm -rf build source kernel/fs/{cifs,fuse,hfsplus,nfs,ntfs,reiserfs,udf,xfs,autofs4,exportfs,isofs,lockd} kernel/arch/arm/oprofile/oprofile.ko kernel/drivers/net/{usb,ethernet,slip,ppp} kernel/drivers/{md,scsi,usb/storage,usb/serial,usb/gadget/g_serial.ko,usb/gadget/g_printer.ko,usb/gadget/g_zero.ko,usb/gadget/g_ether.ko}  kernel/drivers/media/i2c/smia/et8ek8.ko'")
        cmd("bash -c 'rm -rf build source kernel/net/netfilter'")

        os.chdir(self.srcdir)
        print 'rsyncing'
        maemo_chroot = "/media/mmc1p6"
        maemo_scp = "root@maemo:"+maemo_chroot
        cmd("rsync -zavP --delete /tmp/kernel-modules/lib/modules/%s/ %s/lib/modules/%s" % (str, maemo_scp, str))
#        cmd("rsync -zavP --delete maemo_mods " + maemo_scp + "/lib/modules/" + str)
        cmd("rsync -zavP zImage_dtb root@maemo:/opt/boot/")
        # yes, depmod is neccessary.
        cmd("ssh root@maemo 'depmod -b " + maemo_chroot + " " + str + " && u-boot-update-bootmenu'")

    def get_tests(self):
        return [ Boots_emu(), I2C_irq_ok() ]

    def backup_config(self, dir_into):
        cmd("cp dmesg.qemu "+dir_into)
        cmd("cp .config "+dir_into)

#
# Stuff for eldk 
#

class Network_test(Test):
    def prepare(self, iteration):
        self.ip = iteration.machine.target_ip

class Shell_test(Test):
    def run(self, iteration):
        self.machine = iteration.machine
        return self.shell()

class Version_test(Shell_test):
    name = "Tests /image_version on target"

    def shell(self):
        self.machine.cmd_shell("cat /image_version")
        return "pass"

class Expect:
    uboot_clock = 0
    last_output = []

    def wait_msg(self, phase, regexp, timeout):
        signal.alarm(timeout)
        while 1:
            line = self.comms.stdout.readline()
            if not line:
                raise Exception("Reading from device failed. (killall rlogin ?)")
            sys.stdout.write("%s %f " % (phase, time.time()-self.startup))
            sys.stdout.write(line)
            self.last_output.append(line)
            self.log.write(line)
            self.log.flush()
            if re.match(regexp, line):
                print "### Milestone reached: ", phase
                signal.alarm(10*60)
                return
        signal.alarm(10*60)

    def write_target(self, s):
        print "writing: ", s
        self.comms.stdin.write(s)
        self.comms.stdin.flush()

    def cmd_any(self, s, msg, timeout = 10, wait_key = ""):
        self.last_output = []
        key = "Proceed%03d" % self.uboot_clock
        self.uboot_clock += 1
        # time.sleep(1)
        self.write_target(s % key)
        self.wait_msg(msg, wait_key+key, timeout)

    # u-boot does not implement reasonable flow control; try talking slowly
    def cmd_uboot(self, s, timeout = 10):
        self.cmd_any(s + " ; echo %s\n", "u-boot talk", timeout)

    def cmd_shell(self, s, timeout = 10):
        self.cmd_any(s + " ; echo %s\n", "shell talk", timeout)

    def connect(self, s):
        self.startup = time.time()
        self.comms = subprocess.Popen(s, shell=False, stdin=subprocess.PIPE, stdout=subprocess.PIPE)

    def shutdown_hw(self):
        print "stopping connection to hardware"
        self.comms.terminate()
        self.comms.wait()
        cmd("stty sane")

class Bdi(Expect):
    def connect_bdi(self):
        self.log =  open("bdi.chat", "w")
        self.connect([ "telnet", "bdi14" ] )
        self.wait_msg("welcome", ".*terminate the Telnet session", 10)

    def cmd_bdi(self, s, timeout = 10):
        self.cmd_any(s + " ; load %s\r\n", "bdi talk", timeout, ".*Loading ")

class Tftpboot(Expect):
    def power_cycle(self):
        cmd("remote_power "+self.target+" off")
        time.sleep(1)
        cmd("remote_power "+self.target+" on")
        time.sleep(1)

    def bdi_reset(self):
        os.system("(sleep 1; echo reset run; sleep 1) | telnet "+self.bdi)

    def reboot(self):
        self.power_cycle()
        #self.bdi_reset()

    def connect_hw(self, iteration):
        os.chdir(self.srcdir)
        self.log = open("bootmsg.hw", "w")
        self.connect( [ "connect", self.target ] )
        self.reboot()

    def get_tests(self):
        return []

class EldkKernel(Tftpboot):
    def backup_config(self, dir_into):
        cmd("cp .config "+dir_into)
        cmd("cp bootmsg.hw "+dir_into)


class Ipconfig_ok(Search_text):
    name = "Network works, ipconfig gets address"

    def run(self, iteration):
        res = self.search("bootmsg.hw",
              [ "IP-Config: Complete:" ])
        if res: return "pass"
        return "FAIL"

class MMC_ok(Search_text):
    name = "MMC is detected"

    def run(self, iteration):
        res = self.search("bootmsg.hw",
              [ "mmc0: new SD card at address" ])
        if res: return "pass"
        return "FAIL"

class USB_detected(Search_text):
    name = "USB controller is detected"

    def run(self, iteration):
        res = self.search("bootmsg.hw",
              [ "usb usb1: Product: DWC OTG Controller",
                "dwc_otg ffb40000.usb: DWC OTG Controller" ])
        if res: return "pass"
        return "FAIL"

class Socfpga(EldkKernel):
    def __init__(self):
        self.target = "socfpga"
        self.srcdir = "/home/pavel/mainline-altera/linux"
        self.mydir = "/home/pavel/auto"
        self.tftp_dir = "/tftpboot/"+self.target
        self.dtb = "socfpga_cyclone5.dtb"
        self.arch = "arm"
        self.name = "socfpga"
        self.nfsroot_dir = "/work/pavel/root-"+self.target

    def build(self, iteration):
        os.chdir(self.srcdir)
        git_description = pcmd("git describe")
        cmd(""". eldk-switch.sh -r 5.2 armv7a
            ARCH=arm make -j 9 uImage LOADADDR=0x8000 &&
	    ARCH=arm make socfpga_cyclone5.dtb""")
        
        cmd("cp arch/%s/boot/dts/%s %s/%s.%s" %
            ( self.arch, self.dtb, self.tftp_dir, self.dtb, git_description))
        cmd("cp arch/%s/boot/uImage %s/uImage.%s" %
            ( self.arch,           self.tftp_dir, git_description))
        os.chdir(self.tftp_dir)
        cmd("ln -sf uImage.%s uImage" % git_description)
        cmd("ln -sf %s.%s %s" % ( self.dtb, git_description, self.dtb ))

    def run_hw(self, iteration):
        # Altera wants connect _after_ power cycle.
        self.reboot()
        time.sleep(3)
        os.chdir(self.srcdir)
        self.log = open("bootmsg.hw", "w")
        self.connect( [ "connect", self.target ] )
        self.wait_msg("Mounts root", ".*(VFS: Unable to mount root|VFS: Mounted root)", 120)

    def get_tests(self):
        return [ Ipconfig_ok(), MMC_ok(), USB_detected() ]

class Bitbake(Tftpboot):
    def install_copy(self, extension):
        os.chdir(self.tftp_dir)
        cmd("scp "+self.worker_host+":"+self.worker_image+extension+" /work/pavel/root-"+self.target+extension)
        cmd("scp "+self.worker_host+":"+self.deploy_kernel+" uImage.bb")
        cmd("scp "+self.worker_host+":"+self.deploy_uboot+" u-boot.bb")
        cmd("scp "+self.worker_host+":"+self.deploy_dtb+" dtb.bb")

    def install_nfsroot(self):
        cmd("sudo rm -rf /work/pavel/root-"+self.target)
        self.install_copy(".tar.gz")
        cmd("mkdir "+self.nfsroot_dir)
        os.chdir(self.nfsroot_dir)
        cmd("sudo tar xzf ../root-"+self.target+".tar.gz")
        cmd("sudo mkdir ctmp")
        cmd("sudo chmod 1777 ctmp")

    def install_jffs2(self):
        self.install_copy(".jffs2")

    def filesize(self, name):
        statinfo = os.stat(name)
        return statinfo.st_size

    def command(self, c):
        if c == "install-nfsroot":
            return self.install_nfsroot()
        if c == "install-jffs2":
            return self.install_jffs2()
        if c == "clean-all":
            return self.clean_all()
        if c == "cp-root":
            cmd("sudo cp -r meta-novega/root.mods/* "+self.nfsroot_dir)
            return 0
        if c == "swupdate":
            return self.prepare_update()
        if c == "install-uboot":
            return self.install_uboot()
        if c == "test":
            return self.cmdline_test()
        print "Unknown command ", c



# To run on new worker, do:
# prepare directory on the worker, adjust worker_host.
# Copy suitable conf/ from backup.

class VlabBitbake(Bitbake):
    def __init__(self):
        self.target_netmask = "255.255.0.0"
        self.tftp_server = "192.168.1.1"
        self.tftp_dir = "/tftpboot/"+self.target

class MyBitbake(VlabBitbake):
    def __init__(self):
        self.srcdir = "/home/pavel/"+self.target+"/eldk"
        self.mydir = "/home/pavel/auto"
        VlabBitbake.__init__(self)
        self.name = self.target
        self.worker_host = "pavel@sisyphus"
        self.worker_dir = "/work/pavel/build-"+self.target

class P2020(MyBitbake):
    def __init__(self):
        self.target = "p2020rdb"
        MyBitbake.__init__(self)
        self.name = "p2020"
        self.worker_host = "pavel@sisyphus"
        self.srcdir = "/home/pavel/p2020/eldk-v2"
#        self.worker_image = self.worker_dir
        self.machine = "generic-powerpc-e500v2"
        self.bitbake_target = "core-image-p2020rdb"

    def build(self, iteration):
        os.chdir(self.srcdir)
        self.remote_build(iteration)

class Iteration:
    run_emu = 1
    run_hw = 0
    do_build = 1
    alone = 0
    machines = [ N900(), P2020(), Socfpga() ]

    def get_machine(self, name):
        for m in self.machines:
            if name == m.name:
                os.chdir(m.srcdir)
                return m
        if name == "":
            d = os.getcwd()
            for m in self.machines:
                print m.name, d, m.srcdir
                if d == m.srcdir:
                    return m
        # m = Ib8315_NFS()
        raise Exception("Machine not specifed and not in suitable srcdir")
        
    def __init__(self, name):
        print "User asks for ", name
        self.machine = self.get_machine(name)
        self.dir = self.machine.srcdir + "/cur_test"
    
    def mkdir(self):
        mydir = self.machine.mydir
        print "Git ", self.git_version, " time ", self.time
        try:
            os.mkdir(mydir + "/" + self.git_version)
        except: pass
        self.dir = mydir + "/" + self.git_version + "/" + ("%f" % self.time)
        os.mkdir(self.dir)
        try:
            os.unlink("prev_test")
        except: pass
        try:
            os.rename("cur_test", "prev_test")
        except: pass
        os.symlink(self.dir, "cur_test")
        print "Have directory " + self.dir

    def prepare(self):
        self.time = time.time()
        self.git_version = pcmd("git show | head -1")
        self.git_version = self.git_version.split(" ")[1]
        self.diff = pcmd("git diff")
        if self.diff != "":
            print "Diff is non-empty"
            self.git_version = self.git_version + "-dirty"
        self.mkdir()

    def run_tests(self):
        print "Testing in directory " + self.dir
        os.chdir(self.dir)
        out = open(self.dir + "/tests.log", "a")
        for test in self.machine.get_tests():
            print "Test ", test.name
            res = test.run(self)
            print "Result ", res
            msg = test.name + ": " + res + "\n"
            sys.stdout.write(msg)
            out.write(msg)
        out.close()

        print "Test summary:"
        cmd("cat " + self.dir + "/tests.log")

        if 0: # res == "pass":
            out.write("Graphics works on emulator: ?\n")
            out.write("Graphics works on hardware: ?\n")
            os.system("emacs " + self.dir + "/tests.log &")

    def build(self):
        os.chdir(self.machine.srcdir)
        self.prepare()
        self.machine.build(self)

    def run(self):
        if self.do_build:
            self.build()

        if True:
            if self.run_emu:
                self.machine.run_emu(self)
            if self.run_hw:
                self.machine.run_hw(self)
            self.machine.backup_config(self.dir)
            self.run_tests()
        #finally:
            # We want to kill rlogins, etc.
        #    self.machine.shutdown_hw()

def help():
    print 'atest.py [...]'
    sys.exit(2)

def git_branch(s):
    cmd("git checkout "+s)

def pull(s):
    if s == "mainline":
        git_branch("v3.11-rc0-dt")
        cmd("git pull /data/l/clean-cg")
        return
    if s == "next":
        git_branch("linux-next/master")
        cmd("git remote update")
        # This is bad, it merged linux-next/master with previous master.
        #cmd("git merge linux-next/master")
        cmd("git merge v3.11-rc0-dt")

        return

def main(argv):
   try:
      opts, args = getopt.getopt(argv,"hl:ewtm:br",["pull=", "build-on-worker", "goto-worker", "shell-on-worker", "run-emu", "run-hw", "test", "clean-all", "machine", "reboot", "build", "run-full"])
   except getopt.GetoptError:
       help()

   print "have argv ", argv
   print "have args ", args
   if args != [] and args[0][0] != "-":
       machine = args[0]
   else:
       machine = ""
   print "have machine ", machine

   done = 0
   iteration = Iteration(machine)
   for opt, arg in opts:
      if opt == '-h':
          help()
      elif opt in ("-l", "--pull"):
          pull(arg)
      elif opt in ("-r", "--run-full"):
          iteration.run()
          done = 1
      elif opt in ("-e", "--run-emu"):
          iteration.do_build = 0
          iteration.run_emu = 1
          iteration.run_hw = 0
          iteration.run()
          done = 1
      elif opt in ("-w", "--run-hw"):
          iteration.do_build = 0
          iteration.run_emu = 0
          iteration.run_hw = 1
          iteration.run()
          done = 1
      elif opt in ("-b", "--build"):
          iteration.build()
          done = 1
      # oops, this matches --build, too?!"
      elif opt in ("--build-on-worker"):
          print "...this is on worker"
          iteration.machine.build_on_worker()
          print "...done on worker"
          done = 1
      elif opt in ("--goto-worker"):
          iteration.machine.goto_worker()
          done = 1
      elif opt in ("--shell-on-worker"):
          print "...this is on worker"
          iteration.machine.shell_on_worker()
          print "...done on worker"
          done = 1
      elif opt in ("-t", "--test-only"):
          iteration.run_tests()
          done = 1
      elif opt in ("-m", "--machine"):
          iteration.machine.command(arg)
          done = 1
      elif opt in ("--clean-all"):
          iteration.machine.clean_all()
          done = 1
      elif opt in ("--reboot"):
          iteration.machine.reboot()
          done = 1

   if not done:
       iteration.run()

print "atest.py running"
if __name__ == "__main__":
   main(sys.argv[1:])
